=========================
atlassian-util-concurrent
=========================
general purpose concurrency utilities
-------------------------------------

This project contains utility classes that are used by various products and projects inside Atlassian and may have some utility to the world at large.
These are designed to help make it easier to write concurrent code correctly, and generally encapsulate correct usage inside the utility classes.

Included are lazy references, copy-on-write maps, latches, promises, some AtomicX extensions and an asynchronous completion service.

There is more documentation on the wiki_.

Issue Tracking
--------------
Issues are tracked here_

.. _wiki: https://bitbucket.org/atlassian/atlassian-util-concurrent/wiki
.. _here: https://bitbucket.org/atlassian/atlassian-util-concurrent/issues?status=new&status=open
